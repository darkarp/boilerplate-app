#[tokio::main]
async fn main() -> Result<(), Error> {
    dotenv().ok();
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .without_time()
        .init();

    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(_event: LambdaEvent<Value>) -> Result<Value, Error> {
    let mut new_proccessor = Proccessor::new().await.unwrap();
    let count = new_proccessor.run().await.expect("process error");

    Ok(json!({ "message": format!("process , {}!", count) }))
}
