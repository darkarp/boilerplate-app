use async_trait::async_trait;
use core::fmt::Debug;

use crate::error::AppError;
pub mod slack;

#[async_trait(?Send)]
pub trait Producer {
    async fn run(&self, msg: &str) -> Result<(), AppError>;
}

impl Debug for dyn Producer {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Producer{{{:?}}}", self)
    }
}

#[derive(Debug)]
pub struct AppAlert {
    producer: Box<dyn Producer>,
}

impl AppAlert {
    pub async fn producer(&self, msg: &str) -> Result<(), AppError> {
        self.producer.run(msg).await
    }

    pub fn new(producer: Box<dyn Producer>) -> Self {
        AppAlert { producer }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use dotenv::dotenv;
    use slack::Slack;

    #[tokio::test]
    async fn app_alert() {
        dotenv().ok();

        let trading_view_alert_slack = AppAlert::new(Box::new(Slack {
            webhook_url: "test_url".into(),
            client: reqwest::Client::new(),
        }));

        let res = trading_view_alert_slack.producer("msg").await;
        let _ = match res {
            Ok(()) => unimplemented!(),
            Err(error) => {
                assert_eq!(
                    error.to_string(),
                    "builder error: relative URL without a base"
                )
            }
        };
    }
}
