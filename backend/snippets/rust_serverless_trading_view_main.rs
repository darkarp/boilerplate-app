#[tokio::main]
async fn main() -> Result<(), AppError> {
    dotenv().ok();
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .without_time()
        .init();

    let mut new_proccessor = Proccessor::new().await.unwrap();
    let insert_num = new_proccessor.run().await?;
    info!("we process {} records", insert_num);
    Ok(())
}
