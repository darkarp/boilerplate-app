use actix_web::{
    web::{self, ServiceConfig},
    HttpResponse,
};

pub fn service(cfg: &mut ServiceConfig) {
    cfg.route("/status", web::get().to(status_check));
}

async fn status_check() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[cfg(test)]
mod tests {
    use actix_web::{http::StatusCode, middleware, App};

    use super::*;

    #[actix_rt::test]
    async fn status_test() {
        let res = status_check().await;
        assert_eq!(res.status(), StatusCode::OK);
    }

    #[actix_rt::test]
    async fn status_thread() {
        let app = App::new()
            .wrap(
                middleware::DefaultHeaders::new().add(("thread_id", web::Data::new(3).to_string())),
            )
            .configure(service);
        let mut app = actix_web::test::init_service(app).await;
        let req = actix_web::test::TestRequest::get()
            .uri("/status")
            .to_request();
        let res = actix_web::test::call_service(&mut app, req).await;
        assert_eq!(res.status(), StatusCode::OK);
        let data = res
            .headers()
            .get("thread_id")
            .map(|h| h.to_str().ok())
            .flatten();
        assert_eq!(data, Some("3"));
    }
}
