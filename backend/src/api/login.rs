use std::sync::Arc;

use actix_web::{
    web::{self, Data, Json, ServiceConfig},
    HttpResponse,
};
use chrono::{Duration, Utc};
use jsonwebtoken::{encode, EncodingKey, Header};
use models::users::{Claims, LoginUser};

use crate::repository::user_repository::UserRepository;

pub fn service<T: UserRepository + 'static>(cfg: &mut ServiceConfig) {
    cfg.route("/login", web::post().to(login_user::<T>));
}

async fn login_user<T: UserRepository>(
    user: Json<LoginUser>,
    repository: Data<Arc<T>>,
    secret: Data<Arc<String>>,
) -> HttpResponse {
    match repository
        .get_user_check_password(&user.username, &user.password)
        .await
    {
        Ok(user) => {
            let iat = Utc::now().timestamp_nanos();
            let exp = iat + Duration::days(2).num_nanoseconds().unwrap();
            let claims = Claims {
                id: user.id,
                role: user.role,
                iat,
                exp,
            };
            match encode(
                &Header::default(),
                &claims,
                &EncodingKey::from_secret(secret.as_bytes()),
            ) {
                Ok(token) => HttpResponse::Ok().json(token),
                Err(_) => HttpResponse::InternalServerError().finish(),
            }
        }
        Err(err) => HttpResponse::BadRequest().body(err),
    }
}
