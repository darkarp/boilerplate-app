use actix_web::{
    web::{self, Data, Json, Path},
    HttpResponse,
};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use jsonwebtoken::{decode, DecodingKey, Validation};
use models::users::{Claims, NewUser, User, VariableUser};
use std::sync::Arc;
use uuid::Uuid;
use web::ServiceConfig;

use crate::repository::user_repository::UserRepository;
pub fn service<T: UserRepository + 'static>(cfg: &mut ServiceConfig) {
    cfg
        //
        .route("", web::get().to(get_users::<T>))
        .route("/{id}", web::get().to(get_user_by_id::<T>))
        .route("", web::post().to(create_user::<T>))
        .route("/{id}", web::delete().to(delete_user::<T>))
        .route("", web::patch().to(update_user::<T>));

    // TODO update_user
}

async fn get_users<T: UserRepository>(
    repository: Data<Arc<T>>,
    auth: BearerAuth,
    secret: Data<Arc<String>>,
) -> HttpResponse {
    //!
    // todo security
    //!
    match decode::<Claims>(
        &auth.token(),
        &DecodingKey::from_secret(secret.as_bytes()),
        &Validation::default(),
    ) {
        Ok(_token) => HttpResponse::Ok().json(repository.get_users().await),
        Err(err) => HttpResponse::Unauthorized().body(err.to_string()),
    }
}

async fn get_user_by_id<T: UserRepository>(
    user_id: Path<Uuid>,
    repository: Data<Arc<T>>,
) -> HttpResponse {
    //!
    // todo security
    //!
    match repository.get_user_by_id(&user_id).await {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::BadRequest().body(err),
    }
}

pub async fn create_user<T: UserRepository>(
    new_user: Json<NewUser>,
    repository: Data<Arc<T>>,
) -> HttpResponse {
    //!
    // todo security
    //!
    let user = User::new(
        new_user.username.clone(),
        Some(new_user.email.clone()),
        new_user.password.clone(),
    );

    match repository.create_user(&user).await {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::InternalServerError().body(err),
    }
}

async fn update_user<T: UserRepository>(
    user: Json<VariableUser>,
    repository: Data<Arc<T>>,
) -> HttpResponse {
    //!
    // todo security
    //!
    match repository.update_user(&user).await {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::InternalServerError().body(err),
    }
}

async fn delete_user<T: UserRepository>(
    uuid: Path<Uuid>,
    repository: Data<Arc<T>>,
) -> HttpResponse {
    //!
    // todo security
    //!
    match repository.delete_user(&uuid).await {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::NotFound().body(err),
    }
}

#[cfg(test)]
mod tests {
    #[derive(Deserialize)]
    struct UserInResponse {
        id: Uuid,
        username: String,
        email: String,
    }

    use crate::repository::MemoryRepository;

    use super::*;
    use actix_web::{http::StatusCode, test, App};

    use serde::Deserialize;
    #[actix_rt::test]
    async fn get_users_test() {
        // TODO test cases
        assert_eq!(StatusCode::OK, StatusCode::OK);
    }
    #[actix_rt::test]
    async fn get_user_by_id_memory_test() {
        // TODO test cases
        let repo = Arc::new(MemoryRepository::default());
        let app = App::new()
            .app_data(web::Data::new(repo))
            .configure(service::<MemoryRepository>);
        let mut app = actix_web::test::init_service(app).await;
        let req = actix_web::test::TestRequest::get()
            .uri("/bbd05624-f89f-5ff2-b453-5f25755dbb1d")
            .to_request();
        let res = actix_web::test::call_service(&mut app, req).await;
        assert_eq!(res.status(), StatusCode::OK);
        let user: serde_json::Value = test::read_body_json(res).await;
        let user: UserInResponse = serde_json::from_value(user).unwrap();
        assert_eq!(
            user.id,
            Uuid::new_v5(&Uuid::NAMESPACE_DNS, "DefaultName".as_bytes())
        );
        assert_eq!(user.username, "DefaultName");
        assert_eq!(user.email, "DefaultEmail@example.com");
    }
}
