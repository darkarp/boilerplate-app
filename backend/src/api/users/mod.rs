mod users;

use actix_web::web::{self, ServiceConfig};

use crate::repository::user_repository::UserRepository;

pub fn service<T: UserRepository + 'static>(cfg: &mut ServiceConfig) {
    cfg.service(web::scope("/users").configure(users::service::<T>));
}
