use std::sync::Arc;

use actix_web::{
    http,
    web::{self, Data},
    HttpResponse, Responder,
};
use actix_web_httpauth::extractors::bearer::BearerAuth;
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use models::{
    auth::{fetch_access_token, get_me},
    users::{Claims, User},
};
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{Deserialize, Serialize};
use url::Url;
use web::ServiceConfig;

use crate::repository::user_repository::UserRepository;

#[derive(Debug, Serialize, Deserialize)]
struct CallbackQuery {
    code: Option<String>,
    state: Option<String>,
}
pub fn service<T: UserRepository + 'static>(cfg: &mut ServiceConfig) {
    cfg
        // GET routes
        .route("/me", web::get().to(me::<T>))
        .route("/login", web::get().to(login))
        .route("/callback", web::get().to(callback::<T>))
        .route("/logout", web::get().to(logout));
}

async fn me<T: UserRepository>(
    auth: BearerAuth,
    secret: Data<Arc<String>>,
    repository: Data<Arc<T>>,
) -> impl Responder {
    match decode::<Claims>(
        &auth.token(),
        &DecodingKey::from_secret(secret.as_bytes()),
        &Validation::default(),
    ) {
        Ok(token) => {
            if let Ok(user) = repository.get_user_by_id(&token.claims.id).await {
                return HttpResponse::Ok().json(user);
            }
            return HttpResponse::BadRequest().finish();
        }
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

async fn login() -> impl Responder {
    let state: String = thread_rng()
        .sample_iter(Alphanumeric)
        .take(8)
        .map(char::from)
        .collect();

    let mut url = Url::parse("https://discord.com/oauth2/authorize").unwrap();
    let client_id = std::env::var("CLIENT_ID").expect("Error loading client_id");
    let redirect_uri = std::env::var("DISC_REDIR").expect("Error loading redirect_uri");
    let scopes = std::env::var("DISC_SCOPE").expect("Error loading scope");

    url = Url::parse(
        format!(
            "{}?state={}&prompt=none&cliend_it={}&response_type=code&scope={}&redirect_uri={}",
            url, state, client_id, scopes, redirect_uri
        )
        .as_str(),
    )
    .expect("Url parsing failed");

    HttpResponse::Found()
        .append_header((http::header::LOCATION, url.to_string()))
        .finish()
}

async fn callback<T: UserRepository>(
    query: web::Query<CallbackQuery>,
    secret: Data<Arc<String>>,
    repository: Data<Arc<T>>,
) -> impl Responder {
    if let Some(code) = &query.code {
        if let Ok(token) = fetch_access_token(code).await {
            match get_me(&token.token_type, &token.access_token).await {
                Ok(user) => {
                    let new_user = User::new(
                        user.username.clone(),
                        user.email.clone(),
                        token.access_token,
                    );
                    log::info!("New user: {}", &user.tag());
                    log::info!("User info: {:?}", &user);
                    // TODO save auth
                    let iat = Utc::now().timestamp_nanos();
                    let exp = iat + Duration::days(2).num_nanoseconds().unwrap();
                    let claims = Claims {
                        id: new_user.id.clone(),
                        role: new_user.role.clone(),
                        iat,
                        exp,
                    };
                    match encode(
                        &Header::default(),
                        &claims,
                        &EncodingKey::from_secret(secret.as_bytes()),
                    ) {
                        Ok(token) => {
                            //
                            if repository
                                .get_user_check_password(
                                    &new_user.username,
                                    &new_user.password_hash,
                                )
                                .await
                                .is_ok()
                            {
                                return HttpResponse::Found().json(token);
                            }
                            match repository.create_user(&new_user).await {
                                Ok(_) => return HttpResponse::Ok().json(token),
                                Err(_) => return HttpResponse::Unauthorized().body("cheeky ;)"),
                            }
                        }
                        Err(err) => {
                            return HttpResponse::InternalServerError().body(err.to_string())
                        }
                    }
                }
                _ => (),
            }
        }
    }
    HttpResponse::InternalServerError().body("Internal Server Error")
}

async fn logout() -> HttpResponse {
    todo!()
}

#[cfg(test)]
mod tests {
    use actix_web::http::StatusCode;
    #[actix_rt::test]
    async fn get_login_test() {
        // TODO
        assert_eq!(StatusCode::OK, StatusCode::OK);
    }
}
