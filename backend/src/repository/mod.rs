use std::sync::RwLock;

use models::users::User;
use sqlx::PgPool;

pub mod user_repository;

pub struct MemoryRepository {
    users: RwLock<Vec<User>>,
}

impl Default for MemoryRepository {
    fn default() -> Self {
        Self {
            users: RwLock::new(vec![
                // Example first user
                User::new(
                    "DefaultName".to_string(),
                    Some("DefaultEmail@example.com".to_string()),
                    "DefaultPassword".to_string(),
                ),
            ]),
        }
    }
}
pub struct PostgresRepository {
    pool: PgPool,
}
