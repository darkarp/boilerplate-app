use async_trait::async_trait;
use chrono::Utc;
use models::users::{User, VariableUser};
use sqlx::PgPool;
use uuid::Uuid;

use super::{MemoryRepository, PostgresRepository};

#[async_trait]
pub trait UserRepository {
    async fn get_users(&self) -> Result<Vec<User>, String>;
    async fn get_user_by_id(&self, user_id: &Uuid) -> Result<User, String>;
    async fn get_user_check_password(
        &self,
        username: &str,
        password_hash: &str,
    ) -> Result<User, String>;
    async fn create_user(&self, user: &User) -> Result<User, String>;
    async fn update_user(&self, user: &VariableUser) -> Result<User, String>;
    async fn delete_user(&self, user: &Uuid) -> Result<User, String>;
}

#[async_trait]
impl UserRepository for MemoryRepository {
    async fn get_users(&self) -> Result<Vec<User>, String> {
        Ok(self
            .users
            .read()
            .expect("Error reading from repository")
            .to_vec())
    }
    async fn get_user_by_id(&self, user_id: &Uuid) -> Result<User, String> {
        let users = self.users.read().expect("Error getting user by id");
        for user in users.iter() {
            if user.id == *user_id {
                return Ok(user.clone());
            }
        }
        Err("Not found".to_string())
    }

    async fn create_user(&self, user: &User) -> Result<User, String> {
        let mut users = self.users.write().expect("Error creating user");
        users.push(user.to_owned());
        Ok(user.to_owned())
    }

    async fn update_user(&self, user: &VariableUser) -> Result<User, String> {
        if let Some(user) = self
            .users
            .read()
            .expect("Error updating")
            .iter()
            .find(|x| x.id == user.id)
        {
            let mut users = self.users.write().expect("Error updating user");
            users.retain(|x| x.id != user.id);
            users.push(user.to_owned());
            return Ok(user.to_owned());
        }
        Err("Error updating user".to_string())
    }

    async fn delete_user(&self, user_id: &Uuid) -> Result<User, String> {
        let users = self.users.read().expect("Error deleting user - read");
        if let Some(user) = users.iter().find(|x| x.id == *user_id) {
            for user in users.iter() {
                if user.id == *user_id {
                    break;
                }
            }
            self.users
                .write()
                .expect("Error deleting user - write")
                .retain(|x| x.id != *user_id);
            Ok(user.to_owned())
        } else {
            Err("Deletion of user Unsuccessful".to_string())
        }
    }
    async fn get_user_check_password(
        &self,
        _username: &str,
        _password_hash: &str,
    ) -> Result<User, String> {
        todo!()
    }
}

impl PostgresRepository {
    pub async fn from_env() -> sqlx::Result<Self> {
        let url = std::env::var("DATABASE_URL").expect("Error loading postgres url");
        let pool = PgPool::connect(&url).await?;
        Ok(Self { pool })
    }
}

#[async_trait]
impl UserRepository for PostgresRepository {
    async fn get_users(&self) -> Result<Vec<User>, String> {
        let query = "SELECT * FROM users";
        sqlx::query_as::<_, User>(query)
            .fetch_all(&self.pool)
            .await
            .map_err(|x| x.to_string())
    }
    async fn get_user_by_id(&self, user_id: &Uuid) -> Result<User, String> {
        let query = "SELECT * FROM users WHERE id=$1";
        sqlx::query_as::<_, User>(query)
            .bind(user_id)
            .fetch_one(&self.pool)
            .await
            .map_err(|x| x.to_string())
    }

    async fn create_user(&self, user: &User) -> Result<User, String> {
        sqlx::query_as::<_, User>(
            r#"
        INSERT INTO users (id, username, email, password_hash, email_verified, role, active)
        VALUES ($1, $2, $3, $4, $5, $6, $7)
        RETURNING *
        "#,
        )
        .bind(&user.id)
        .bind(&user.username)
        .bind(&user.email)
        .bind(&user.password_hash)
        .bind(&user.email_verified)
        .bind(&user.role)
        .bind(&user.active)
        .fetch_one(&self.pool)
        .await
        .map_err(|x| x.to_string())
    }

    async fn get_user_check_password(
        &self,
        username: &str,
        password_hash: &str,
    ) -> Result<User, String> {
        sqlx::query_as::<_, User>(
            r#"
        SELECT * FROM users
        WHERE (username=$1 OR email=$1) AND (password_hash=$2)
        "#,
        )
        .bind(&username)
        .bind(&password_hash)
        .fetch_one(&self.pool)
        .await
        .map_err(|x| x.to_string())
    }

    async fn update_user(&self, user: &VariableUser) -> Result<User, String> {
        sqlx::query_as::<_, User>(
            r#"
        UPDATE users
        SET 
            username=COALESCE($1, username), 
            email=COALESCE($2, email), 
            password_hash=COALESCE($3, password_hash), 
            email_verified=COALESCE($4, email_verified), 
            role=COALESCE($5, role), 
            active=COALESCE($6, active), 
            updated_at=COALESCE($7, updated_at)
        WHERE id=$8
        RETURNING *;"#,
        )
        .bind(&user.username)
        .bind(&user.email)
        .bind(&user.password_hash)
        .bind(&user.email_verified)
        .bind(&user.role)
        .bind(&user.active)
        .bind(Utc::now())
        .bind(&user.id)
        .fetch_one(&self.pool)
        .await
        .map_err(|x| x.to_string())
    }

    async fn delete_user(&self, user_id: &Uuid) -> Result<User, String> {
        let query = "DELETE FROM users WHERE id=$1 RETURNING *";
        sqlx::query_as::<_, User>(query)
            .bind(user_id)
            .fetch_one(&self.pool)
            .await
            .map_err(|x| x.to_string())
    }
}
