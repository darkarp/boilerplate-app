mod api;
mod repository;

use actix_web::{middleware, web::Data, App, HttpServer};
use env_logger::Builder;
use log::LevelFilter;
use std::env;
use std::process::exit;
use std::sync::{
    atomic::{AtomicU16, Ordering},
    Arc,
};

use crate::api::{login, oauth, status, users};
use crate::repository::PostgresRepository;

fn build_logger() {
    Builder::new().filter(None, LevelFilter::Info).init();
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // init logger
    build_logger();

    // init env vars
    dotenv::dotenv().ok();

    // building address
    let port = std::env::var("PORT").unwrap_or("8000".to_string());
    let host = std::env::var("HOST").unwrap_or("0.0.0.0".to_string());
    let address = format!("{}:{}", host, port);
    log::info!("Starting server on {}", address);

    // this identified threads for debugging
    let thread_counter = Arc::new(AtomicU16::new(1));

    // db repo
    let repo = PostgresRepository::from_env()
        .await
        .expect("Error creating repository");
    let repo = Arc::new(repo);

    // secret jwt
    let secret = Arc::new(env::var("SECRET_KEY").expect("Error loading secret key"));

    // starting the server
    HttpServer::new(move || {
        let thread_index = thread_counter.fetch_add(1, Ordering::SeqCst);
        log::info!("Starting thdread {}", thread_index);
        // starting the services
        App::new()
            .app_data(Data::new(repo.clone()))
            .app_data(Data::new(secret.clone()))
            .wrap(middleware::DefaultHeaders::new().add(("thread_id", thread_index.to_string())))
            .wrap(middleware::NormalizePath::trim())
            // health
            .configure(status::service)
            // authentication required
            .configure(login::service::<PostgresRepository>)
            .configure(oauth::service::<PostgresRepository>)
            .configure(users::service::<PostgresRepository>)
    })
    .bind(&address)
    .unwrap_or_else(|err| {
        log::error!("{}", err);
        exit(1)
    })
    .run()
    .await
}
