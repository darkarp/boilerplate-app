# Boilerplate Backend

## Docker compose ⇾ Postgres + backend
```bash
docker-compose up -d --build
```
### Run migrations before using db: `sqlx mig run --database-url postgres://postgres:postgres@localhost:5433/boilerplate_db`
> To installl sqlx-cli for the above command: `cargo install sqlx-cli`

## Test requests in `reqs.http`

## Test OAuth with `GET /discord/login` in browser