use reqwest::{Client, Url};
use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AuthUser {
    pub id: String,
    pub discord: DiscordUser,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct DiscordUser {
    pub id: String,
    pub username: String,
    pub discriminator: String,
    pub avatar: Option<String>,
    pub bot: Option<bool>,
    pub sytem: Option<bool>,
    pub mfa_enabled: Option<bool>,
    pub locale: Option<String>,
    pub verified: Option<bool>,
    pub email: Option<String>,
    pub flags: Option<u128>,
    pub premium_type: Option<u8>,
    pub public_flags: Option<u128>,
}

impl DiscordUser {
    pub fn tag(&self) -> String {
        format!("{}#{}", self.username, self.discriminator)
    }
}

pub async fn get_me(token_type: &str, token: &str) -> Result<DiscordUser, reqwest::Error> {
    Client::new()
        .get("https://discord.com/api/users/@me")
        .header("Authorization", format!("{} {}", token_type, token))
        .send()
        .await?
        .json()
        .await
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AccessTokenResponse {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: u64,
    pub refresh_token: String,
    pub scope: String,
}
pub async fn fetch_access_token(code: &str) -> Result<AccessTokenResponse, reqwest::Error> {
    let mut form: HashMap<&str, &str> = HashMap::new();
    let client_id = std::env::var("CLIENT_ID").expect("Error loading client_id");
    let client_secret = std::env::var("CLIENT_SECRET").expect("Error loading client_secret");
    let redirect_uri = std::env::var("DISC_REDIR").expect("Error loading redirect_uri");
    let scopes = std::env::var("DISC_SCOPE").expect("Error loading scope");
    form.insert("client_id", &client_id);
    form.insert("client_secret", &client_secret);
    form.insert("grant_type", "authorization_code");
    form.insert("code", code);
    form.insert("redirect_uri", &redirect_uri);
    form.insert("scope", &scopes);

    let url = Url::parse("https://discord.com/api/oauth2/token").unwrap();
    let client = Client::new();

    client.post(url).form(&form).send().await?.json().await
}
