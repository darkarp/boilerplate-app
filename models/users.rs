use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use uuid::Uuid;
use validator_derive::Validate;

#[derive(Serialize, Deserialize, Clone, Debug, FromRow)]
pub struct User {
    pub id: Uuid,
    pub username: String,
    pub email: Option<String>,
    #[serde(skip_serializing)]
    pub password_hash: String,
    #[serde(skip_serializing)]
    pub email_verified: bool,
    #[serde(skip_serializing)]
    pub role: Role,
    #[serde(skip_serializing)]
    pub active: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: Option<DateTime<Utc>>,
}

impl User {
    pub fn new(username: String, email: Option<String>, password_hash: String) -> Self {
        Self {
            id: Uuid::new_v5(&Uuid::NAMESPACE_DNS, username.as_bytes()),
            username,
            email,
            password_hash,
            active: true,
            created_at: Utc::now(),
            updated_at: None,
            email_verified: false,
            role: Role::User,
        }
    }
}
#[derive(Serialize, Deserialize, Clone, Debug, FromRow)]
pub struct VariableUser {
    pub id: Uuid,
    pub username: Option<String>,
    pub email: Option<String>,
    pub password_hash: Option<String>,
    pub email_verified: Option<bool>,
    pub role: Option<Role>,
    pub active: Option<bool>,
    #[serde(skip_serializing)]
    pub created_at: Option<DateTime<Utc>>,
    #[serde(skip_serializing)]
    pub updated_at: Option<DateTime<Utc>>,
}

#[derive(Clone, Debug, Deserialize, Validate)]
pub struct NewUser {
    #[validate(length(min = 4))]
    pub username: String,
    #[validate(email)]
    pub email: String,
    #[validate(length(min = 8))]
    pub password: String,
}

#[derive(Clone, Debug, Deserialize, Validate)]
pub struct LoginUser {
    pub username: String,
    pub password: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Claims {
    pub id: Uuid,
    pub role: Role,
    pub iat: i64,
    pub exp: i64,
}

#[derive(Serialize, Deserialize, Clone, Debug, sqlx::Type)]
#[sqlx(type_name = "user_role", rename_all = "lowercase")]
pub enum Role {
    Admin,
    User,
}
