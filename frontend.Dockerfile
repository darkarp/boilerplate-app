FROM rust:latest as build

WORKDIR /app
COPY . .

# run tests
RUN cd backend; cargo test
# build dev
RUN cd backend; cargo build

RUN apt install libssl-dev -y

# installing dependencies
RUN cargo install trunk
RUN cargo install wasm-bindgen-cli

RUN cd backend; trunk serve

