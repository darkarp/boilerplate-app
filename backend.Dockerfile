FROM rust:latest as build

WORKDIR /app
# cache steps
COPY . .
# run tests
RUN cd backend; cargo test
# build prod
RUN cd backend; cargo build --release


CMD ["/app/backend/target/release/boilerplate_backend"]